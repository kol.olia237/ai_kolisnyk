import matplotlib.pyplot as plt
import numpy as np
import sklearn.metrics as sm
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures

m = 100
X = np.linspace(-3, 3, m)
y = np.sin(X) + np.random.uniform(-0.5, 0.5, m)

# Побудова графіка Лінійна регресія
fig, ax = plt.subplots()
ax.scatter(X, y, color='red')
plt.title("Лінійна регресія")
plt.show()

print("X[1], y[1]",X[1], y[1])

poly_features = PolynomialFeatures(degree=3, include_bias=False)
X_poly = poly_features.fit_transform(np.array(X).reshape(-1, 1))

linear_regression = linear_model.LinearRegression()
linear_regression.fit(X_poly, y)
print("linear_regression.intercept_ ", np.round(linear_regression.intercept_, 2))
print("linear_regression.coef_ ", np.round(linear_regression.coef_, 2))
y_pred = linear_regression.predict(X_poly)

print("\nR2 = ", sm.r2_score(y, y_pred))

fig, ax = plt.subplots()
ax.scatter(X, y, color='red')
plt.plot(X, y_pred, "*", color='blue', linewidth=4)
plt.title("Поліноміальна регресія")
plt.show()
