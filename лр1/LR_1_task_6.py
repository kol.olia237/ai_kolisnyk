import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

from utilities import visualize_classifier

input_file = 'data_multivar_nb.txt'

data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=3)

classifier_SVC = SVC()
classifier_NB = GaussianNB()

classifier_SVC.fit(X_train, y_train)
classifier_NB.fit(X_train, y_train)

y_pred_SVC = classifier_SVC.predict(X_test)
y_pred_NB = classifier_NB.predict(X_test)

accuracy_SVC = 100.0 * (y_test == y_pred_SVC).sum() / X_test.shape[0]
print("Accuracy of SVC classifier =", round(accuracy_SVC, 2), "%")

accuracy_NB = 100.0 * (y_test == y_pred_NB).sum() / X_test.shape[0]
print("Accuracy of NB classifier =", round(accuracy_NB, 2), "%")

visualize_classifier(classifier_SVC, X_test, y_test)
visualize_classifier(classifier_NB, X_test, y_test)

num_folds = 3

print("SVC:")
accuracy_values_SVC = cross_val_score(classifier_SVC,
                                      X, y, scoring='accuracy', cv=num_folds)
print("Accuracy SVC: " + str(round(100 * accuracy_values_SVC.mean(), 2)) + "%")

precision_values_SVC = cross_val_score(classifier_SVC,
                                       X, y, scoring='precision_weighted', cv=num_folds)
print("Precision SVC: " + str(round(100 * precision_values_SVC.mean(), 2)) + "%")

recall_values_SVC = cross_val_score(classifier_SVC,
                                    X, y, scoring='recall_weighted', cv=num_folds)
print("Recall SVC: " + str(round(100 * recall_values_SVC.mean(), 2)) + "%")

f1_values_SVC = cross_val_score(classifier_SVC,
                                X, y, scoring='f1_weighted', cv=num_folds)
print("F1 SVC: " + str(round(100 * f1_values_SVC.mean(), 2)) + "%")

print("NB:")
accuracy_values_NB = cross_val_score(classifier_NB,
                                     X, y, scoring='accuracy', cv=num_folds)
print("Accuracy NB: " + str(round(100 * accuracy_values_NB.mean(), 2)) + "%")

precision_values_NB = cross_val_score(classifier_NB,
                                      X, y, scoring='precision_weighted', cv=num_folds)
print("Precision NB: " + str(round(100 * precision_values_NB.mean(), 2)) + "%")

recall_values_NB = cross_val_score(classifier_NB,
                                   X, y, scoring='recall_weighted', cv=num_folds)
print("Recall NB: " + str(round(100 * recall_values_NB.mean(), 2)) + "%")

f1_values_NB = cross_val_score(classifier_NB,
                               X, y, scoring='f1_weighted', cv=num_folds)
print("F1 NB: " + str(round(100 * f1_values_NB.mean(), 2)) + "%")
